json.extract! order, :id, :name, :person_id, :created_at, :updated_at
json.url order_url(order, format: :json)
